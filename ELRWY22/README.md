# On Defeating Graph Analysis of Anonymous Transactions

## Paper

    On Defeating Graph Analysis of Anonymous Transactions
    Christoph Egger, Russell W. F. Lai, Viktoria Ronge, Ivy K. Y. Woo, Hoover H. F. Yin
    In: Proc. Priv. Enhancing Technol. 2022(3)

A Preprint is available at [IACR ePrint](https://eprint.iacr.org/2022/132).

## This Code

This directory contains the simulation code. 
In particular, [PETS22-Submission.ipynb](PETS22-Submission.ipynb) contains full simulation code while [binom.pck](binom.pck) and [regular.pck](regular.pck) contain precomputed results which can be used by switching from `if True` to `if False` in the notebook.
We make use of [ipyparallel](https://ipython.readthedocs.io/en/stable/) to speed up running time, a full run takes approximately 20 hours on a 2 * 14 * 2 CPU system (~2018 Intel Xeon).
